from sqlalchemy import Boolean, Column, Float, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Country(Base):
    __tablename__ = 'country'

    id = Column(Integer, primary_key=True)
    name = Column(String(75), nullable=False)
    iso_3166_alpha2 = Column(String(2), unique=True, nullable=False)

    def __repr__(self):
        return "<Country(name='{0}', iso_3166_alpha2='{1}')>".format(
            self.name, self.iso_3166_alpha2
        )


class Port(Base):
    __tablename__ = 'port'

    id = Column(Integer, primary_key=True)
    name = Column(String(150), nullable=False)
    country_id = Column(Integer, ForeignKey('country.id'), nullable=False)

    # Port details.
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)
    port_authority = Column(String(150))
    address = Column(String(300))
    phone = Column(String(20))
    fax = Column(String(20))
    email = Column(String(100))
    website = Column(String(100))
    un_locode = Column(String(6))
    port_type = Column(String(50))
    port_size = Column(String(25))
    max_draft = Column(String(2))

    # General information.
    first_entry_port = Column(Boolean)
    eta_required = Column(Boolean)
    medical_facility = Column(Boolean)

    # Harbor characteristics.
    harbor_size = Column(String(25))
    shelter = Column(String(25))
    max_vessel_size = Column(String(75))
    harbor_type = Column(String(50))
    turning_area = Column(Boolean)
    good_holding_ground = Column(Boolean)

    # Entrance restrictions.
    tide_restrictions = Column(Boolean)
    overhead_restrictions = Column(Boolean)
    swell_restrictions = Column(Boolean)

    # Water depth.
    channel_depth = Column(String(150))
    cargo_pier_depth = Column(String(150))
    mean_tide_depth = Column(String(150))
    anchorage_depth = Column(String(150))
    oil_terminal_depth = Column(String(150))

    # Pilotage.
    pilotage_compulsory = Column(Boolean)
    pilotage_available = Column(Boolean)
    pilotage_advisable = Column(Boolean)
    pilotage_local_assist = Column(Boolean)

    # Tugs.
    tugs_assist = Column(Boolean)
    tugs_salvage = Column(Boolean)

    # Quarantine.
    quarantine_pratique = Column(Boolean)
    quarantine_deratt_cert = Column(Boolean)

    # Communications.
    telephone = Column(Boolean)
    radio = Column(Boolean)
    air = Column(Boolean)
    telegraph = Column(Boolean)
    radio_telephone = Column(Boolean)
    rail = Column(Boolean)

    # Loading & unloading.
    wharves = Column(Boolean)
    med_moor = Column(Boolean)
    ice = Column(Boolean)
    anchor = Column(Boolean)
    beach = Column(Boolean)

    # Lifts & cranes.
    gt100_ton_lifts = Column(Boolean)
    i50_100_ton_lifts = Column(Boolean)
    i25_49_ton_lifts = Column(Boolean)
    i0_24_ton_lifts = Column(Boolean)
    fixed_cranes = Column(Boolean)
    mobile_cranes = Column(Boolean)
    floating_cranes = Column(Boolean)

    # Port services.
    longshore_service = Column(Boolean)
    electrical_repair_service = Column(Boolean)
    steam_service = Column(Boolean)
    electrical_service = Column(Boolean)
    navigation_equipment_service = Column(Boolean)

    # Supplies.
    provisioning_supplies = Column(Boolean)
    fuel_oil_supplies = Column(Boolean)
    deck_supplies = Column(Boolean)
    water_supplies = Column(Boolean)
    diesel_oil_supplies = Column(Boolean)
    engine_supplies = Column(Boolean)

    # Repairs and service facilities.
    ship_repairs = Column(String(25))
    marine_railroad_size = Column(String(25))
    degauss = Column(String(25))
    drydock_size = Column(String(25))
    garbage_disposal = Column(Boolean)
    dirty_ballast = Column(Boolean)

    def __repr__(self):
        return "<Port(name='{0}', country='{1}'>".format(
            self.name, self.country.name
        )
