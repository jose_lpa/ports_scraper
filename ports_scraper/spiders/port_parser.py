import logging


logger = logging.getLogger(__name__)


class PortParser:
    def __init__(self, scrapy_response, port_name, country):
        self.response = scrapy_response
        self.port_name = port_name
        self.country_name = country.name

        # Initialize Port model information data dict.
        self.port_information = {
            'name': self.port_name,
            'country_id': country.id
        }

        # Make a list of all `parse_` methods, so we can call them when needed.
        self.parsers = []
        for attribute in dir(self):
            _attr = getattr(self, attribute)
            if callable(_attr) and attribute.startswith('parse_'):
                self.parsers.append(_attr)

    def _get_from_table(self, table_id, caption):
        """
        Given a CSS ID for a table in the HTML and the data caption string, it
        searches the document for the caption via XPath and returns the pure
        data.
        """
        return self.response.xpath(
            "//table[@id='{0}']/tr/td[contains(., '{1}')]"
            "/following-sibling::td/text()".format(table_id, caption)
        ).extract_first()

    def _get_link_text_from_table(self, table_id, caption):
        """
        Special method for those table cells that contain an HTTP link.

        Given a CSS ID for a table in the HTML and the data caption string, it
        searches the document for the caption via XPath and returns the text in
        the data link.
        """
        return self.response.xpath(
            "//table[@id='{0}']/tr/td[contains(., '{1}')]"
            "/following-sibling::td/a/text()".format(table_id, caption)
        ).extract_first()

    def _get_true_or_false(self, table_id, caption):
        """
        To be used in those data fields that are written in the HTML as 'Yes' or
        'No'. Will return ``True`` or ``False``, respectively, or ``None`` if
        there were no data at all.
        """
        data = self._get_from_table(table_id, caption)

        if data is None:
            return

        # Clean blankspaces and normalize data.
        data = data.lower().strip()
        if not data:
            return

        # Sanity check.
        assert data == 'yes' or data == 'no', "{0} is not 'yes' or 'no'".format(
            data
        )

        if data == 'yes':
            return True
        elif data == 'no':
            return False

    def _get_coordinates(self):
        """
        Helper method to obtain the DMS port coordinates and transform them into
        decimal coordinates.
        """
        latitude = self._get_from_table('port_det', 'Latitude:')
        longitude = self._get_from_table('port_det', 'Longitude:')

        # Transform DMS coordinates into decimal degrees.
        def _get_decimal_coordinates(coordinates):
            degrees, minutes, seconds, direction = coordinates.split(' ')
            degrees = int(degrees.replace('º', ''))
            minutes = int(minutes.replace("'", ''))
            seconds = int(seconds.replace("'", ''))

            return seconds/3600 + minutes/60 + degrees

        latitude = _get_decimal_coordinates(latitude)
        longitude = _get_decimal_coordinates(longitude)

        logger.info(
            "Acquired coordinates '%s' - '%s' for %s port in %s.",
            latitude,
            longitude,
            self.port_name,
            self.country_name
        )

        return latitude, longitude

    def parse_port_details(self):
        """ Port Detail section. """
        latitude, longitude = self._get_coordinates()

        self.port_information.update({
            'latitude': latitude,
            'longitude': longitude,
            'port_authority': self._get_from_table(
                'port_det', 'Port Authority:'
            ),
            'address': self._get_from_table('port_det', 'Address:'),
            'phone': self._get_from_table('port_det', 'Phone:'),
            'fax': self._get_from_table('port_det', 'Fax:'),
            'email': self._get_link_text_from_table('port_det', 'Email:'),
            'website': self._get_link_text_from_table('port_det', 'Web Site:'),
            'un_locode': self._get_from_table('port_det', 'UN/LOCODE:'),
            'port_type': self._get_from_table('port_det', 'Port Type:'),
            'port_size': self._get_from_table('port_det', 'Port Size:'),
            'max_draft': self._get_from_table('port_det', 'Max Draft:'),
        })

        logger.info(
            "Parsed 'Port Details' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_general_information(self):
        """ General Information section. """
        self.port_information.update({
            'first_entry_port': self._get_true_or_false(
                'gen_inf', 'First Port of Entry:'
            ),
            'first_entry_port': self._get_true_or_false(
                'gen_inf', 'ETA Message Required:'
            ),
            'first_entry_port': self._get_true_or_false(
                'gen_inf', 'Medical Facilities:'
            ),
        })

        logger.info(
            "Parsed 'General Information' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_harbor_characteristics(self):
        """ Harbor Characteristics section. """
        self.port_information.update({
            'harbor_size': self._get_from_table('har_char', 'Harbor Size:'),
            'shelter': self._get_from_table('har_char', 'Shelter:'),
            'max_vessel_size': self._get_from_table(
                'har_char', 'Maximum Vessel Size:'
            ),
            'harbor_type': self._get_from_table('har_char', 'Harbor Type:'),
            'turning_area': self._get_true_or_false(
                'har_char', 'Turning Area:'
            ),
            'good_holding_ground': self._get_true_or_false(
                'har_char', 'Good Holding Ground:'
            ),
        })

        logger.info(
            "Parsed 'Harbor Characteristics' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_entrance_restrictions(self):
        """ Entrance Restrictions section. """
        self.port_information.update({
            'tide_restrictions': self._get_true_or_false(
                'en_res', 'Tide:'
            ),
            'overhead_restrictions': self._get_true_or_false(
                'en_res', 'Overhead Limit:'
            ),
            'swell_restrictions': self._get_true_or_false(
                'en_res', 'Swell:'
            ),
        })

        logger.info(
            "Parsed 'Entrance Restrictions' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_water_depth(self):
        """ Water Depth section. """
        self.port_information.update({
            'channel_depth': self._get_from_table('wat_dep', 'Channel:'),
            'cargo_pier_depth': self._get_from_table('wat_dep', 'Cargo Pier:'),
            'mean_tide_depth': self._get_from_table('wat_dep', 'Mean Tide:'),
            'harbor_type': self._get_from_table('wat_dep', 'Harbor Type:'),
            'anchorage_depth': self._get_from_table('wat_dep', 'Anchorage:'),
            'oil_terminal_depth': self._get_from_table(
                'wat_dep', 'Oil Terminal:'
            ),
        })

        logger.info(
            "Parsed 'Water Depth' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_pilotage(self):
        """ Pilotage section. """
        self.port_information.update({
            'pilotage_compulsory': self._get_true_or_false(
                'pil', 'Compulsory:'
            ),
            'pilotage_available': self._get_true_or_false('pil', 'Available:'),
            'pilotage_advisable': self._get_true_or_false('pil', 'Advisable:'),
            'pilotage_local_assist': self._get_true_or_false(
                'pil', 'Local Assist:'
            ),
        })

        logger.info(
            "Parsed 'Pilotage' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_tugs(self):
        """ Tugs section. """
        self.port_information.update({
            'tugs_assist': self._get_true_or_false('tugs', 'Assist:'),
            'tugs_salvage': self._get_true_or_false('tugs', 'Salvage:'),
        })

        logger.info(
            "Parsed 'Tugs' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_quarantine(self):
        """ Quarantine section. """
        self.port_information.update({
            'quarantine_pratique': self._get_true_or_false('quar', 'Pratique:'),
            'quarantine_deratt_cert': self._get_true_or_false(
                'quar', 'Deratt Cert:'
            ),
        })

        logger.info(
            "Parsed 'Quarantine' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_communications(self):
        """ Communications section. """
        self.port_information.update({
            'telephone': self._get_true_or_false('comm', 'Telephone:'),
            'radio': self._get_true_or_false('comm', 'Radio:'),
            'air': self._get_true_or_false('comm', 'Air:'),
            'telegraph': self._get_true_or_false('comm', 'Telegraph:'),
            'radio_telephone': self._get_true_or_false('comm', 'Radio Tel:'),
            'rail': self._get_true_or_false('comm', 'Rail:'),
        })

        logger.info(
            "Parsed 'Communications' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_loading_unloading(self):
        """ Loading & Unloading section. """
        self.port_information.update({
            'wharves': self._get_true_or_false('lu', 'Warves:'),
            'med_moor': self._get_true_or_false('lu', 'Med Moor:'),
            'ice': self._get_true_or_false('lu', 'Ice:'),
            'anchor': self._get_true_or_false('lu', 'Anchor:'),
            'beach': self._get_true_or_false('lu', 'Beach:'),
        })

        logger.info(
            "Parsed 'Loading & Unloading' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_lifts_cranes(self):
        """ Lifts & Cranes section. """
        self.port_information.update({
            'gt100_ton_lifts': self._get_true_or_false('lc', '100+ Ton Lifts:'),
            'i50_100_ton_lifts': self._get_true_or_false(
                'lc', '50-100 Ton Lifts:'
            ),
            'i25_49_ton_lifts': self._get_true_or_false(
                'lc', '25-49 Ton Lifts:'
            ),
            'i0_24_ton_lifts': self._get_true_or_false('lc', '0-24 Ton Lifts:'),
            'fixed_cranes': self._get_true_or_false('lc', 'Fixed Cranes:'),
            'mobile_cranes': self._get_true_or_false('lc', 'Mobile Cranes:'),
            'floating_cranes': self._get_true_or_false(
                'lc', 'Floating Cranes:'
            ),
        })

        logger.info(
            "Parsed 'Lifts & Cranes' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_port_services(self):
        """ Port Services section. """
        self.port_information.update({
            'longshore_service': self._get_true_or_false(
                'por_ser', 'Longshore:'
            ),
            'electrical_repair_service': self._get_true_or_false(
                'por_ser', 'Electrical Repair:'
            ),
            'steam_service': self._get_true_or_false(
                'por_ser', 'Steam:'
            ),
            'electrical_service': self._get_true_or_false(
                'por_ser', 'Electrical:'
            ),
            'navigation_equipment_service': self._get_true_or_false(
                'por_ser', 'Navigation Equipment:'
            ),
        })

        logger.info(
            "Parsed 'Port Services' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_supplies(self):
        """ Supplies section. """
        self.port_information.update({
            'provisioning_supplies': self._get_true_or_false(
                'sup', 'Provisions:'
            ),
            'fuel_oil_supplies': self._get_true_or_false('sup', 'Deck:'),
            'water_supplies': self._get_true_or_false('sup', 'Water:'),
            'diesel_oil_supplies': self._get_true_or_false(
                'sup', 'Diesel Oil:'
            ),
            'engine_supplies': self._get_true_or_false('sup', 'Engine:'),
        })

        logger.info(
            "Parsed 'Supplies' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def parse_repairs_services(self):
        """ Repairs, Drydock, Railway & Other Services section. """
        self.port_information.update({
            'ship_repairs': self._get_from_table('rep', 'Ship Repairs:'),
            'marine_railroad_size': self._get_from_table(
                'rep', 'Marine Railroad Size:'
            ),
            'degauss': self._get_from_table('rep', 'Degauss:'),
            'drydock_size': self._get_from_table('rep', 'Drydock Size:'),
            'garbage_disposal': self._get_true_or_false(
                'rep', 'Garbage Disposal:'
            ),
            'dirty_ballast': self._get_true_or_false('rep', 'Dirty Ballast:'),
        })

        logger.info(
            "Parsed 'Repairs, Drydock, Railway & Other' for %s port in %s.",
            self.port_name,
            self.country_name
        )

    def get_port_information(self):
        """
        Executes all the parsing methods to fill the `self.port_information`
        dictionary and returns it.

        This method is the actually only one to be called from an instance of
        ``PortParser`` to get ALL the information of the scraped port.
        """
        for parser in self.parsers:
            parser()

        return self.port_information
