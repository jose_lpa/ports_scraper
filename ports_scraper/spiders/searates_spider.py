import logging

import scrapy

from ports_scraper.database.connection import Session
from ports_scraper.database.models import Country, Port
from .port_parser import PortParser


logger = logging.getLogger(__name__)


class SeaRatesSpider(scrapy.Spider):
    name = 'searates'

    def __init__(self, name=None, **kwargs):
        super(SeaRatesSpider, self).__init__(name, **kwargs)

        # Initialize database session.
        self.session = Session()

    def _get_or_create(self, model, **kwargs):
        instance = self.session.query(model).filter_by(**kwargs).first()
        if instance:
            logger.info('%s - %s already registered.', model.__name__, kwargs)
            return instance
        else:
            logger.info('Creating new %s: %s.', model.__name__, kwargs)
            instance = model(**kwargs)
            self.session.add(instance)
            self.session.commit()
            return instance

    def start_requests(self):
        urls = [
            'https://www.searates.com/maritime/',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse_port(self, response):
        parser = PortParser(
            scrapy_response=response,
            port_name=response.meta['port_name'],
            country=response.meta['country']
        )

        # Parse the response and create new Port DB object.
        port_info = parser.get_port_information()

        port = Port(**port_info)
        self.session.add(port)
        self.session.commit()

        logger.info('Stored data for port %s.', response.meta['port_name'])

    def parse_country(self, response):
        # Get the country object.
        country = self.session.query(Country).filter_by(
            name=response.meta['country_name']
        ).first()
        logger.info("Retrieved country '%s'", country.name)

        # Get the port links.
        port_links = response.xpath("//ul[@id='plist']/li")

        for link in port_links:
            port_name = link.xpath('span/text()').extract_first()
            follow_url = link.xpath('a/@href').extract_first()
            logger.info(
                "Parsing port %s in '%s'...",
                port_name,
                follow_url,
            )

            request = response.follow(follow_url, callback=self.parse_port)
            request.meta['country'] = country
            request.meta['port_name'] = port_name

            yield request

    def parse(self, response):
        countries_selector = response.xpath(
            "//select[@id='country-content']/option"
        )

        # Scrape and store the available countries data.
        for country in countries_selector:
            # Get or create the scraped Country.
            country_data = {
                'iso_3166_alpha2': country.xpath('@value').extract_first(),
                'name':  country.xpath('text()').extract_first(),
            }
            self._get_or_create(Country, **country_data)

        country_links = response.xpath("//ul[@id='clist']/li/a")

        # Follow the country links and scrape each country ports data.
        for link in country_links:
            country_name = link.xpath('text()').extract_first()
            follow_url = link.xpath('@href').extract_first()
            logger.info(
                "Parsing ports for %s in '%s'...",
                country_name,
                follow_url
            )

            request = response.follow(follow_url, callback=self.parse_country)
            request.meta['country_name'] = country_name.strip()

            yield request
