"""
Initialize the PostgreSQL database, creating the necessary tables specified in
``database.models`` module, by running this script.
"""
from ports_scraper.database.connection import engine
from ports_scraper.database.models import Base


Base.metadata.create_all(engine)
