# Ports scraper

A web spider that crawls the [searates.com](https://www.searates.com/maritime) site to extract world maritime ports information and store it in a PostgreSQL database.

### Requirements

* Python 3.5+
* PostgreSQL 9.5+

### Installation

1. Install project dependencies by the usual `pip install -r requirements` (you may want to do it in a Python virtual environment).
2. Make sure you have a PostgreSQL database properly set up, and configure project to use it in `ports_scraper.settings` module by using the `DATABASE_*` settings (you will know how to do that ;) ).
3. Create the database tables by running `python initialize_db.py` within the `ports_scraper/` directory.

### Usage

If you followed the three installation steps, you should be ready to go now. Just run `scrapy crawl searates` in your shell and sit tight while `searates_spider` does its job. Soon, your database will be populated with all world maritime ports :)
